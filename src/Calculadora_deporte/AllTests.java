package Calculadora_deporte;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ calculadoraCarb.class, calculadoraFat.class, calculadoraGanar.class, calculadoraMantener.class,
		calculadoraPerdre.class, calculadoraProteines.class })
public class AllTests {

}
