package Calculadora_deporte;

import java.util.Scanner;
/**
 * Clase STOP FAST FOOD
 *
 *STOP FAST FOOD
 *Programa que calcula las calorias y los macronutrientes que tiene que ingerir una persona segun su objetivo.
 *
 * @author Alejandro Aranda
 * @version 1.0
 */

public class calculadoraDeporte {
	static Scanner sc = new Scanner (System.in);
	
	//Main
	 
    /**
     * Gestiona el progrma segun el objetivo de la persona 
     */

	public static void main(String[] args) {
		
		int actividad=0;
		int obj;
		double pes=0, act;
		double kg = 0, cal=0;
		int x=0;
		int pro = 0, carb = 0, fat = 0;
	   // System.out.println("STOP FAST FOOD");
		int o=sc.nextInt();
		
		while (o>0) {
		
	    	obj=menuobj();
	    	pes=pes(kg);
	    	int entreno = menuact();
			
	    	if(obj==1) {
	    		
	    		x=6;
	    		cal=(int)guanyar(pes, obj,x, entreno);
	    		pro=(int) proteines(obj, cal);
	    		carb=(int) carb(obj, cal);
	    		fat=(int) fat(obj, cal);
	    		

	    	}
	    	if(obj==2) {
	    		
	    		x=2;
	    		cal=(int)mantindre(pes, obj,x, entreno);
	    		pro=(int) proteines(obj, cal);
	    		carb=(int) carb(obj, cal);
	    		fat=(int) fat(obj, cal);
	    		
	    	
	    	}
	    	if(obj==3) {
	    		
	    		x=0;
	    		cal=(int)perdre(pes, obj,x, entreno);
	    		pro=(int) proteines(obj, cal);
	    		carb=(int) carb(obj, cal);
	    		fat=(int) fat(obj, cal);
	    		
	    		
	    	}
	    
    		System.out.println(cal);
    		System.out.println(((double)(pro/10000)));
    		System.out.println(((double)(carb/10000)));
    		System.out.println(((double)(fat/10000)));
    		
    		o--;
		}
	}
	
	//Metodos publicos
	 
    /**
     * Pregunta el objetivo de la persona
     * @return opobj objetivo de la persona
              
     */
	
	
	public static int menuobj()
	{
		int opobj;
	    //System.out.println("Quin es el teu objectiu?");
	    //System.out.println("1. Guanyar pes");
	   // System.out.println("2. Mantindre el pes");
	    //System.out.println("3. Perdre pes");
	    opobj=sc.nextInt();
		return opobj;
}
	//Metodos publicos
	 
    /**
     * Pregunta el numero de entrenamientos de la persona
     * @return opact entrenamientos de la persona
              
     */
	public static int menuact()
	{
		int opact;
	    //System.out.println("Quantes vegades entrenes?");
	   // System.out.println("1. 1-3 vegades a la semana");
	   // System.out.println("2. 4-5 vegades a la semana");
	    //System.out.println("3. 6 o mes cops a la semana");
	    opact=sc.nextInt();
		return opact;
}
	//Metodos publicos
	 
    /**
     * Pregunta el peso de la persona en un rango definido
     * @param kg Peso de la persona
     * @return pes Peso de la persona
              
     */
	public static double pes(double kg)
	{
		boolean correcto = false;
		double pes1 = 0;
		
		
			try {
				//System.out.print("Introdueix un numero entre 35 kg i 200 kg: ");
				pes1 = sc.nextInt();
				if (pes1 >= 35  && pes1 < 200)
					correcto = true;
			}
			catch (Exception e) {
				//System.out.println("Error, s'esperava un numero entre 35 cm i 200 cm");
				sc.nextLine();
			}
		
		return  pes1;
}
	//Metodos publicos
	 
    /**
     * Calcula las calorias de la persona si desea ganar peso
     * @param pes peso de la persona
     * @param obj objetivo de la persona
     * @param x variable del objetivo
     * @param entreno entrenos de la persona
     * @return kcal calorias de la persona 
     
              
     */
	public static double guanyar(double pes, int obj, int x, int entreno)
	{
		
		int y=0;
		double kcal=0;
		
    	if(entreno==1) {
    		y=0;
    		kcal=(10+x+y)*(pes*2.2);
    		
    	}
    	if(entreno==2) {
    		y=2;
    		kcal=(10+x+y)*(pes*2.2);
    	}
    	if(entreno==3) {
    		y=4;
    		kcal=(12+x+y)*(pes*2.2);
    	}
		
		
		
		return  kcal;
}
	//Metodos publicos
	 
    /**
     * Calcula las calorias de la persona si desea mantener peso
     * @param pes peso de la persona
     * @param obj objetivo de la persona
     * @param x variable del objetivo
     * @param entreno entrenos de la persona
     * @return kcal calorias de la persona 
     
              
     */
	public static double mantindre( double pes, int obj, int x, int entreno)
	{
		
		int y=0;
		double kcal=0;
		
    	if(entreno==1) {
    		y=0;
    		kcal=(10+x+y)*(pes*2.2);
    		
    	}
    	if(entreno==2) {
    		y=2;
    		kcal=(10+x+y)*(pes*2.2);
    	}
    	if(entreno==3) {
    		y=4;
    		kcal=(12+x+y)*(pes*2.2);
    	}
		
		
		
		return  kcal;
}
	//Metodos publicos
	 
    /**
     * Calcula las calorias de la persona si desea perder peso
     * @param pes peso de la persona
     * @param obj objetivo de la persona
     * @param x variable del objetivo
     * @param entreno entrenos de la persona
     * @return kcal calorias de la persona 
     
              
     */
	public static double perdre(double pes, int obj, int x, int entreno)
	{
		int y=0;
		double kcal=0;
		
    	if(entreno==1) {
    		y=0;
    		kcal=(10+x+y)*(pes*2.2);
    		
    	}
    	if(entreno==2) {
    		y=2;
    		kcal=(10+x+y)*(pes*2.2);
    	}
    	if(entreno==3) {
    		y=4;
    		kcal=(12+x+y)*(pes*2.2);
    	}
		
		
		
		return  kcal;
}
	//Metodos publicos
	 
    /**
     * Calcula la cantidad de proteinas de la persona 
     * @param obj objetivo de la persona
     * @param cal calorias de la persona
     * @return pro proteinas de la persona 
     
              
     */
	public static double proteines(int obj, double cal)
	{
		int m=0;
		double pro=0;
		
    	if(obj==1) {
    		m=25;
    		pro=((m*100)*cal)/4;
    		
    	}
    	if(obj==2) {
    		m=30;
    		pro=((m*100)*cal)/4;
    		}
    	if(obj==3) {
    		m=35;
    		pro=((m*100)*cal)/4;    	
    		}
		
		
		
		return  pro;
}
	//Metodos publicos
	 
    /**
     * Calcula la cantidad de carbohidratos de la persona 
     * @param obj objetivo de la persona
     * @param cal calorias de la persona
     * @return carb proteinas de la persona 
     
              
     */
	public static double carb(int obj, double cal)
	{
		int m=0;
		double car=0;
		
    	if(obj==1) {
    		m=55;
    		car=((m*100)*cal)/4;
    		
    	}
    	if(obj==2) {
    		m=40;
    		car=((m*100)*cal)/4;
    		}
    	if(obj==3) {
    		m=25;
    		car=((m*100)*cal)/4;    	
    		}
		
		
		
		return  car;
}
	//Metodos publicos
	 
    /**
     * Calcula la cantidad de grasas de la persona 
     * @param obj objetivo de la persona
     * @param cal calorias de la persona
     * @return fat proteinas de la persona             
     */
	public static double fat(int obj, double cal)
	{
		int m=0;
		double fat=0;
		
    	if(obj==1) {
    		m=20;
    		fat=((m*100)*cal)/9;
    		
    	}
    	if(obj==2) {
    		m=30;
    		fat=((m*100)*cal)/9;
    		}
    	if(obj==3) {
    		m=40;
    		fat=((m*100)*cal)/9;    	
    		}
		
		
		
		return  fat;
}
}