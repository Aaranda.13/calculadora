package Calculadora_deporte;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


@RunWith(Parameterized.class)

public class calculadoraGanar {
	
	 private double num1;
	    private int num2;
	    private int num3;
	    private int num4;
	    private int resul;
	    
	    public calculadoraGanar (double pes, int obj, int x, int entreno, int resul) {
	        this.num1 = pes;
	        this.num2 = obj;
	        this.num3 = x;
	        this.num4 = entreno;
	        this.resul = resul;
	    }
	    
	    @Parameters
	    public static Collection<Object[]> numeros() {
	        return Arrays.asList(new Object[][] {
	            {75, 1, 6, 1, 2640},
	            {80, 1, 6, 2, 3168},
	            {60, 1, 6, 3, 2904},
	            {65, 1, 6, 1, 2288},
	            {55, 1, 6, 3, 2662},
	          
	        });
	        
	    }
	    @Test
	    public void testDiv() {
	        int kcal = (int) calculadoraDeporte.guanyar(num1, num2, num3, num4);
	        assertEquals(resul,  kcal);
	    }

}
