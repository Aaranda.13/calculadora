package Calculadora_deporte;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
    
@RunWith(Parameterized.class)

public class calculadoraMantener {
	 private double num1;
	    private int num2;
	    private int num3;
	    private int num4;
	    private int resul;
	    
	    public calculadoraMantener (double pes, int obj, int x, int entreno, int resul) {
	        this.num1 = pes;
	        this.num2 = obj;
	        this.num3 = x;
	        this.num4 = entreno;
	        this.resul = resul;
	    }
	    
	    @Parameters
	    public static Collection<Object[]> numeros() {
	        return Arrays.asList(new Object[][] {
	        	 	{75, 2, 2, 1, 1980},
		            {80, 2, 2, 2, 2464},
		            {60, 2, 2, 3, 2376},
		            {78, 2, 2, 1, 2059},
		            {87, 2, 2, 3, 3445},
	          
	        });
	        
	    }
	    @Test
	    public void testDiv() {
	        int kcal = (int) calculadoraDeporte.mantindre(num1, num2, num3, num4);
	        assertEquals(resul,  kcal);
	    }

}
