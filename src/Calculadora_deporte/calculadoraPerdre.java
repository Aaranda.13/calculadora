package Calculadora_deporte;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
@RunWith(Parameterized.class)
public class calculadoraPerdre {
	 private double num1;
	    private int num2;
	    private int num3;
	    private int num4;
	    private int resul;
	    
	    public calculadoraPerdre (double pes, int obj, int x, int entreno, int resul) {
	        this.num1 = pes;
	        this.num2 = obj;
	        this.num3 = x;
	        this.num4 = entreno;
	        this.resul = resul;
	    }
	    
	    @Parameters
	    public static Collection<Object[]> numeros() {
	        return Arrays.asList(new Object[][] {
	        	{75, 3, 0, 1, 1650},
	            {80, 3, 0, 2, 2112},
	            {60, 3, 0, 3, 2112},
	            {78, 3, 0, 1, 1716},
	            {87, 3, 0, 3, 3062},
	        });
	        
	    }
	    @Test
	    public void testDiv() {
	        int kcal = (int) calculadoraDeporte.perdre(num1, num2, num3, num4);
	        assertEquals(resul,  kcal);
	    }


}
