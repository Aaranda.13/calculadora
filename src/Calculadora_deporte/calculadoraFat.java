package Calculadora_deporte;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
@RunWith(Parameterized.class)

public class calculadoraFat {
	private int num1;
    private double num2;
   
    private int resul;
    
    public calculadoraFat (int obj, double cal,int resul) {
        this.num1 = obj;
        this.num2 = cal;
       
        this.resul = resul;
    }
    
    @Parameters
    public static Collection<Object[]> numeros() {
        return Arrays.asList(new Object[][] {
        	{1, 3000, 666666},
            {2, 3000, 1000000},
            {3, 3000, 1333333},
           {1, 2500, 555555},
           {3, 2000, 888888},
         
          
        });
        
    }
    @Test
    public void testDiv() {
        int carb = (int) calculadoraDeporte.fat(num1, num2);
        assertEquals(resul,  carb);
    }

}
