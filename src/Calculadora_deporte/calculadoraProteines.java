package Calculadora_deporte;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
@RunWith(Parameterized.class)

public class calculadoraProteines {
	 private int num1;
	    private double num2;
	   
	    private int resul;
	    
	    public calculadoraProteines (int obj, double cal,int resul) {
	        this.num1 = obj;
	        this.num2 = cal;
	       
	        this.resul = resul;
	    }
	    
	    @Parameters
	    public static Collection<Object[]> numeros() {
	        return Arrays.asList(new Object[][] {
	            {1, 3000, 1875000},
	            {2, 3000, 2250000},
	            {3, 3000, 2625000},
	            {1, 2500, 1562500},
	            {3, 2000, 1750000},
	         
	          
	        });
	        
	    }
	    @Test
	    public void testDiv() {
	        int pro = (int) calculadoraDeporte.proteines(num1, num2);
	        assertEquals(resul,  pro);
	    }


}
