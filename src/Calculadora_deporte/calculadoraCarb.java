package Calculadora_deporte;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
@RunWith(Parameterized.class)
public class calculadoraCarb {
	 private int num1;
	    private double num2;
	   
	    private int resul;
	    
	    public calculadoraCarb (int obj, double cal,int resul) {
	        this.num1 = obj;
	        this.num2 = cal;
	       
	        this.resul = resul;
	    }
	    
	    @Parameters
	    public static Collection<Object[]> numeros() {
	        return Arrays.asList(new Object[][] {
	        	 {1, 3000, 4125000},
		            {2, 3000, 3000000},
		            {3, 3000, 1875000},
		            {1, 2500, 3437500},
		            {3, 2000, 1250000},
	         
	          
	        });
	        
	    }
	    @Test
	    public void testDiv() {
	        int carb = (int) calculadoraDeporte.carb(num1, num2);
	        assertEquals(resul,  carb);
	    }


}
